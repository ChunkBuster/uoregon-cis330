//
// Created by Mason on 2/16/2018.
//

#include "Cell.hpp"
#include <map>
#include "wolf.hpp"
#include "sheep.hpp"
#include "farmer.hpp"
//cell.hpp: Animal *_animal;
Cell::Cell() : _animal(nullptr), _next(nullptr) {

}

//cell.hpp: Animal *_animal;
Cell::Cell(Animal *animal) : _animal(animal), _next(animal) {

}

void Cell::set_contents(Animal *a){
    if(_animal != a && _animal != nullptr)
        delete(_animal);
    _animal = a;
    _next = a;
}

Animal *Cell::get_contents() const{
    return _animal;
}

void Cell::change(Animal *animal){
    _next = animal;
}

void Cell::update(){
    set_contents(_next);
}

void Cell::take_turn(std::vector<Cell*> neighbors) {

    static std::string allAnimals[] = {"S", "W", "F"};
    //This was all going to be done with polymorphism but I just honestly ran out of time
    std::map<std::string, int> popMap;
    for(auto n : neighbors)
        if(n->get_contents() != nullptr)
            popMap[n->_animal->name()]++;

    if(_animal){
        //Sheep logic
        if(_animal->name().compare("S") == 0){
            if(popMap["W"] > 0){
                change(nullptr);
                return;
            }
            if(popMap["S"] > 3){
                change(nullptr);
                return;
            }
        }

        //Wolf logic
        if(_animal->name().compare("W") == 0){
            if(popMap["F"] > 0){
                change(nullptr);
                return;
            }
            if(popMap["S"] > 3){
                change(nullptr);
                return;
            }

            if(popMap["S"] == 0){
                change(nullptr);
                return;
            }
        }


        //Farmer logic
        if(_animal->name().compare("F") == 0) {
            std::vector<Cell*> emptyCells;
            for(auto n : neighbors)
                if(n->get_contents() == nullptr)
                    emptyCells.push_back(n);

            if(emptyCells.size() > 0){
                change(nullptr);
                emptyCells[rand() % emptyCells.size()]->change(new farmer());
                return;
            }
        }
    }else{
        //Reproduction logic
        for(std::string a : allAnimals)
        {
            if (popMap[a] == 3) {
               if(a.compare("S") == 0){
                   change(new sheep());
                   return;
               }else if(a.compare("W") == 0){
                    change(new wolf());
                    return;
                }else if(a.compare("F") == 0){
                   change(new farmer());
                   return;
               }
            }
        }
    }
}

std::ostream& operator<<(std::ostream& os, const Cell& c)
{
    c.write(os);
    return os;
};