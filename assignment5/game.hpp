//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_GAME_HPP
#define ASSIGNMENT5_GAME_HPP
#include <vector>
#include <iostream>
#include"Cell.hpp"

class Game{
public:
    Game(int sizex, int sizey);
    void play_game(int turns);
    //Get cell at a given position
    Cell *get_cell(int x, int y);

    std::ostream &write(std::ostream &dest) const {
        for(int x = 0; x < _sizex; x++){
            for(int y = 0; y < _sizex; y++){
               dest << *_cells[x][y];
            }
            dest << std::endl;
        }
        return dest;
    }

    void update();

    std::vector<Cell*> get_neighbors(int x, int y);

private:
    std::vector<std::vector<Cell*>> _cells;
    int _sizex;
    int _sizey;
    inline int normx(int x){
        if(x < 0)
            x = _sizex + x % _sizex;
        else if (x >= _sizex)
            x = x % _sizex;
        return x;
    }
    inline int normy(int y){
        if(y < 0)
            y = _sizey + y % _sizey;
        else if (y >= _sizey)
            y = y % _sizey;
        return y;
    }
};

std::ostream& operator<<(std::ostream& os, const Game& g);

#endif //ASSIGNMENT5_GAME_HPP
