//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_ANIMAL_H
#define ASSIGNMENT5_ANIMAL_H
#include <string>
#include <vector>
class Cell;

class Animal {

public:
    Animal();
    Animal(std::string name);
    virtual std::string name() const = 0;

    std::ostream &write(std::ostream &dest) const {
        dest << name();
    }
protected:

};
std::ostream& operator<<(std::ostream& os, const Animal& a);

#endif //ASSIGNMENT5_ANIMAL_H
