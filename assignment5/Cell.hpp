//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_CELL_H
#define ASSIGNMENT5_CELL_H
#include <iostream>
#include <vector>
#include "Animal.hpp"

class Cell {
public:
    Cell();
    Cell(Animal* animal);
    Animal *get_contents() const ;
    void take_turn(std::vector<Cell*> neighbors);
    void change(Animal *animal);
    void update();

    std::ostream &write(std::ostream &dest) const {
        if(_animal)
            dest << *_animal;
        else
            dest << '.';
    }
private:
    Animal *_animal;
    Animal *_next;
    void set_contents(Animal *a);

};

std::ostream& operator<<(std::ostream& os, const Cell& c);

#endif //ASSIGNMENT5_CELL_H
