//
// Created by Mason on 2/16/2018.
//
#include <time.h>
#include <iostream>
#include "game.hpp"

int main() {
    srand (time(NULL));
    int numx, numy = 0;
    std::cout << "Please enter the size of the grid (int int):" << std::endl;
    std::cin >> numx;
    std::cin >> numy;

    int steps = 0;
    std::cout << "Please enter the number of steps (int):" << std::endl;
    std::cin >> steps;
    Game game(numx, numy);
    game.play_game(steps);
}