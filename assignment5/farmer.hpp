//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_FARMER_H
#define ASSIGNMENT5_FARMER_H
#include "animal.hpp"
#include <string>

class farmer : public Animal {
public:
    farmer();
protected:
    std::string name() const override;
};


#endif //ASSIGNMENT5_FARMER_H
