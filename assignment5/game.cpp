#include <iostream>
#include "game.hpp"
#include <vector>
#include "wolf.hpp"
#include "sheep.hpp"
#include "farmer.hpp"

void Game::play_game(int turns){
    while(turns-- > 0){
        std::cout << *this << std::endl;
        update();
    }
}

void Game::update(){
   for(int x = 0; x < _sizex; x++)
       for(int y = 0; y < _sizex; y++)
           get_cell(x, y)->take_turn(get_neighbors(x, y));

    for(auto row : _cells)
        for (auto c : row)
            c->update();
}

Cell *Game::get_cell(int x, int y) {
    x = normx(x);
    y = normy(y);
    return _cells[x][y];
}

Game::Game(int sizex, int sizey) : _cells(), _sizex(sizex), _sizey(sizey) {

    for(int i = 0; i < sizey; i++){
        _cells.push_back(std::vector<Cell*>());
        for (int j = 0; j < sizey; j++){
            int type = rand() % 4;
            Animal *animal;
            switch(type){
                case 0: animal = new sheep(); break;
                case 1: animal = new wolf(); break;
                case 2: animal = new farmer(); break;
                default: animal = nullptr; break;
            }
            _cells[i].push_back(new Cell(animal));
        }
    }
}



//l, lu, u, ru, r, dr, d, ld
std::vector<Cell*> Game::get_neighbors(int x, int y){
    std::vector<Cell*> neighbors;
    neighbors.push_back(get_cell(x - 1, y));
    neighbors.push_back(get_cell(x - 1, y + 1));
    neighbors.push_back(get_cell(x, y + 1));
    neighbors.push_back(get_cell(x + 1, y + 1));
    neighbors.push_back(get_cell(x + 1, y));
    neighbors.push_back(get_cell(x + 1, y - 1));
    neighbors.push_back(get_cell(x, y - 1));
    neighbors.push_back(get_cell(x - 1, y - 1));
    return neighbors;
}

std::ostream& operator<<(std::ostream& os, const Game& g)
{
    g.write(os);
    return os;
}
