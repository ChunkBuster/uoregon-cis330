//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_WOLF_H
#define ASSIGNMENT5_WOLF_H
#include "Animal.hpp"

class wolf : public Animal {
public:
    wolf();
protected:
    std::string name() const override;
};


#endif //ASSIGNMENT5_WOLF_H
