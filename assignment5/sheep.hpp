//
// Created by Mason on 2/16/2018.
//

#ifndef ASSIGNMENT5_SHEEP_H
#define ASSIGNMENT5_SHEEP_H

#include <string>
#include "animal.hpp"
class sheep : public Animal{
public:
    sheep();
protected:
    std::string name() const override;
};


#endif //ASSIGNMENT5_SHEEP_H
