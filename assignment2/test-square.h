#ifndef TEST_TEST_SQUARE_H
#define TEST_TEST_SQUARE_H
#define SQUARE_MIN 2
#define SQUARE_MAX 10

int getSquareSize();

#endif //TEST_TEST_SQUARE_H
