#include <stdio.h>
#include "test-triangle.h"
#ifdef TEST_TRIANGLE
#include <stdlib.h>
#include "triangle.h"
int main()
{
    int **triangle = malloc(sizeof(int**));
    int triangleSize = getTriangleSize();

    allocateNumberTriangle(triangleSize, &triangle);
    initializeNumberTriangle(triangleSize, triangle);
    printNumberTriangle(triangleSize, triangle);
    deallocateNumberTriangle(triangleSize, &triangle);
    printf("Done!");
}
#endif

/*Gets the triangle size from the user*/
int getTriangleSize() {
    while (1) {
        printf("\nPlease enter the height of the triangle [%d-%d]:", TRIANGLE_MIN, TRIANGLE_MAX);
        int inputNumber = -1;
        scanf("%d", &inputNumber);
        if (inputNumber == -1 || inputNumber < TRIANGLE_MIN || inputNumber > TRIANGLE_MAX) {
            printf("Invalid input.");
            while (getchar() != '\n');
            continue;
        }
        return inputNumber;
    }
}