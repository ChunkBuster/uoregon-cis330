#include <stdio.h>
#include <stdlib.h>
#include "square.h"

/* Allocate a square of size "size" (a 2-D array of int) */
void allocateNumberSquare(const int size, int ***square) {
    *square = (int **) malloc(sizeof(int *) * size);
    for (int i = 0; i < size; i++) {
        (*square)[i] = malloc(sizeof(int) * size);
    }
}

/* Initialize the 2-D square array */
void initializeNumberSquare(const int size, int **square) {
    for (int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++) {
            square[x][y] = x;
        }
    }
}

/* Print a formatted square */
void printNumberSquare(const int size, int **square) {
    for (int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++) {
            printf("%d ", square[x][y]);
        }
        printf("\n");
    }
}

/* Free the memory for the 2-D square array */
void deallocateNumberSquare(const int size, int ***square) {
    for (int i = 0; i < size; i++) {
        free((*square)[i]);
    }
    free(*square);
}
