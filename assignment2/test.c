#include <stdio.h>
#include "square.h"
#include "triangle.h"
#include "diamond.h"
#include "test-square.h"
#include "test-triangle.h"
#include "test-diamond.h"

typedef void(*shapeFunc)(const int size, int ***);
typedef void(*constShapeFunc)(const int size, int **);
void runShape(int size,
              const shapeFunc allocateFunc, const constShapeFunc initializeFunc,
              const constShapeFunc printFunc, const shapeFunc deallocateFunc) {
    int **allocated = NULL;
    allocateFunc(size, &allocated);
    initializeFunc(size, allocated);
    printFunc(size, allocated);
    deallocateFunc(size, &allocated);
}

int main() {
    int size = 0;
    size = getSquareSize();
    runShape(size, allocateNumberSquare, initializeNumberSquare, printNumberSquare, deallocateNumberSquare);
    size = getTriangleSize();
    runShape(size, allocateNumberTriangle, initializeNumberTriangle, printNumberTriangle, deallocateNumberTriangle);
    size = getDiamondSize();
    runShape(size, allocateNumberSquare, initializeNumberSquare, printNumberDiamond, deallocateNumberSquare);
}
