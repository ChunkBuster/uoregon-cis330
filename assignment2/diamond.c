#include <stdio.h>
#include "diamond.h"

int diamondWidthAtY(int y, int size) {
    //int fullWidth = size * 2;
    //y -= 1;
    int value = (y * 2) + 1;
    int midpoint = (size / 2) + 1;
    if (y >= midpoint) {
        value = (size - (value - size));
    }
    return value;
}

/* Print a formatted diamond */
void printNumberDiamond(const int size, int **diamond) {
    for (int y = 0; y < size; y++) {
        //printf("|%d|", (diamondWidthAtY(size - y, size)) / 2);
        int currentChar = 0;
        //for(int i = 0; i < (size - diamondWidthAtY(size - y, size) + 1); i++){
        for (int i = 0; i < size - (diamondWidthAtY(y, size)); i++) {
            printf(" ");
        }
        for (int x = 0; x < diamondWidthAtY(y, size); x++) {
            printf("%d ", diamond[x][currentChar++]);
        }
        printf("\n");
    }
}
