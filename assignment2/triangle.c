#include <stdio.h>
#include <stdlib.h>
#include "triangle.h"

int triangleWidthAtY(int y) {
    return (y * 2) + 1;
}

/* Allocate a triangle of size "size" (a 2-D array of int) */
void allocateNumberTriangle(const int size, int ***triangle) {
    *triangle = (int **) malloc(sizeof(int *) * size);
    for (int y = 0; y < size; y++) {
        (*triangle)[y] = malloc((sizeof(int) * triangleWidthAtY(size)) + 1);
    }
}

/* Initialize the 2-D triangle array */
void initializeNumberTriangle(const int size, int **triangle) {
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < triangleWidthAtY(size); x++) {
            triangle[y][x] = x;
        }
    }
}

/* Print a formatted triangle */
void printNumberTriangle(const int size, int **triangle) {
    for (int y = 0; y < size; y++) {
        int currentChar = 0;
        for (int i = 0; i < ((triangleWidthAtY(size - (y)) - 2) / 2); i++) {
            printf("  ");
        }
        for (int x = 0; x < triangleWidthAtY(y); x++) {
            printf("%d ", triangle[y][currentChar++]);
        }
        if(y < size + 1)
            printf("\n");
    }
}

/* Free the memory for the 2-D triangle array */
void deallocateNumberTriangle(const int size, int ***triangle) {
    for (int y = 0; y < size; y++) {
        free((*triangle)[y]);
    }
    free(*triangle);
}
