#include <stdio.h>
#include "test-diamond.h"

#ifdef TEST_DIAMOND
#include <stdlib.h>
#include "diamond.h"
#include "square.h"
int main()
{
    int **diamond = malloc(sizeof(int**));
    int diamondSize = getDiamondSize();
    allocateNumberSquare(diamondSize, &diamond);
    initializeNumberSquare(diamondSize, diamond);
    printNumberDiamond(diamondSize, diamond);
    deallocateNumberSquare(diamondSize, &diamond);
}
#endif

/*gets the diamond size from the user*/
int getDiamondSize() {
    while (1) {
        printf("\nPlease enter the size for the diamond [%d-%d], odd:", DIAMOND_MIN, DIAMOND_MAX);
        int inputNumber = -1;
        scanf("%d", &inputNumber);
        if (inputNumber == -1 || inputNumber < DIAMOND_MIN || inputNumber > DIAMOND_MAX || inputNumber % 2 == 0) {
            printf("Invalid input.");
            while (getchar() != '\n');
            continue;
        }
        return inputNumber;
    }
}
