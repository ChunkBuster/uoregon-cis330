#ifndef TEST_TEST_DIAMOND_H
#define TEST_TEST_DIAMOND_H
#define DIAMOND_MIN 3
#define DIAMOND_MAX 9

int getDiamondSize();

#endif //TEST_TEST_DIAMOND_H
