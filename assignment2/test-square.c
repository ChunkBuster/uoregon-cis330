#include <stdio.h>
#include "test-square.h"
#ifdef TEST_SQUARE
#include <stdlib.h>
#include "square.h"
int main()
{
    int **square = malloc(sizeof(int**));
    int squareSize = getSquareSize();
    allocateNumberSquare(squareSize, &square);
    initializeNumberSquare(squareSize, square);
    printNumberSquare(squareSize, square);
    deallocateNumberSquare(squareSize, &square);
}
#endif

/*Gets the square size from the user*/
int getSquareSize() {
    while (1) {
        printf("\nPlease enter the size for the square [%d-%d]:",
               SQUARE_MIN, SQUARE_MAX);
        int inputNumber = -1;
        scanf("%d", &inputNumber);
        if (inputNumber == -1 || inputNumber < SQUARE_MIN || inputNumber > SQUARE_MAX) {
            printf("Invalid input.");
            while (getchar() != '\n');
            continue;
        }
        return inputNumber;
    }
}