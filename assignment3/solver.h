#ifndef MAZE_SOLVER_H
#define MAZE_SOLVER_H
#include "mazedef.h"
#include "vector.h"

typedef struct{
    Direction currentDir;
    Maze *targetMaze;
    int stepsTaken;
    Vector2 position;
    int verbose;
    int drawPath;
} Solver;

//Allocate a new solver and return it
Solver *newSolver(const Vector2 initPos, Maze * const m);
//Free a solver
void freeSolver(Solver *s);
//Rotate a solver by a given amount.
void rotateSolver(Solver *const s, const int rotation);
//Get the input vector offset by d
Vector2 moveInDir(Vector2 v, const Direction d, const int distance);
//Move Move a solver in the direction it is currently facing, and print the move to the console
void moveSolver(Solver *const s, const int distance);
//Preform an iteration of solving the maze. Returns 0 if unsolved, and 1 if solved.
int solveMaze(Solver *const s);

#endif //MAZE_SOLVER_H
