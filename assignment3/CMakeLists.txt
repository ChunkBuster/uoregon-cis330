cmake_minimum_required(VERSION 3.0)

# Use the flags below for all builds and report the build type
set(CMAKE_C_FLAGS "-g -std=c11 -Wall")
message("CMAKE_BUILD_TYPE is ${CMAKE_BUILD_TYPE}")

project(maze)
set(MAZE_SOURCES maze.c mazedef.c solver.c vector.c )
add_executable(maze.exe ${MAZE_SOURCES})

set(MAZE_GEN_SOURCES mazedef.c solver.c vector.c mazegen.c)
add_executable(mazegen.exe ${MAZE_GEN_SOURCES})

add_custom_target(memcheck_maze)
add_compile_options(memcheck_maze -O0)
add_dependencies(memcheck_maze maze.exe)
add_custom_command(TARGET memcheck_maze
        POST_BUILD
        COMMAND valgrind --leak-check=yes --track-origins=yes ./maze.exe maze_input.txt
        VERBATIM
        )

add_custom_target(memcheck_mazegen)
add_compile_options(memcheck_mazegen -O0)
add_dependencies(memcheck_mazegen mazegen.exe)
add_custom_command(TARGET memcheck_mazegen
        POST_BUILD
        COMMAND valgrind --leak-check=yes --track-origins=yes ./mazegen.exe 100
        VERBATIM
        )

add_custom_target(memcheck)
add_dependencies(memcheck memcheck_maze)
add_dependencies(memcheck memcheck_mazegen)



