#ifndef MAZE_VECTOR_H
#define MAZE_VECTOR_H

//Enum representing direction
typedef enum Direction { UP, RIGHT, DOWN, LEFT} Direction;

typedef struct{
    int x;
    int y;
} Vector2;

//Get the absolute direction given a direction relative to locality
Direction relativeDir(const Direction locality, const Direction d);
//Print the string representation of a dir to the console.
void printDir(const Direction d);

#endif //MAZE_VECTOR_H
