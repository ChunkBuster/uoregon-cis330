==1847== Memcheck, a memory error detector
==1847== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==1847== Using Valgrind-3.11.0 and LibVEX; rerun with -h for copyright info
==1847== Command: ./maze.exe maze_input.txt
==1847== Parent PID: 1846
==1847== 
==1847== error calling PR_SET_PTRACER, vgdb might block
==1847== 
==1847== HEAP SUMMARY:
==1847==     in use at exit: 0 bytes in 0 blocks
==1847==   total heap usage: 134 allocs, 134 frees, 8,554 bytes allocated
==1847== 
==1847== All heap blocks were freed -- no leaks are possible
==1847== 
==1847== For counts of detected and suppressed errors, rerun with: -v
==1847== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
