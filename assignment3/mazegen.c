#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "mazedef.h"
#include "solver.h"

#define MAX_STEPS 200
#define EDGE_SIZE 2
#define EMPTY_SPACE_CHANCE 3
#define ROTATION_CHANCE 12
#define EDGE_HIT_FRAC 3
int edgesHit = 0;

//Carve a path through the maze. This guarantees at least one valid path.
int carvePath(Solver *const s, const Maze *m){
	setCharAtPos(m, s->position, EMPTY);
	moveSolver(s, 1);
	int rot = 0;
	//If hit edge
	if(s->position.x >= m->size - 1 || s->position.x < 1 ||
			s->position.y >= m->size - 1 || s->position.y < 1)
	{
		edgesHit++;
		//If below the min number of edge hits, move back, rotate randomly left or right, and continue
		if(edgesHit < m->size / EDGE_HIT_FRAC){
			moveSolver(s, -1);
			rot = (rand() % 3) + 1;
		}else{
			setCharAtPos(m, s->position, EMPTY);
			moveSolver(s, 1);
			setCharAtPos(m, s->position, EMPTY);
			return 1;
		}
	}else{
		//randomly rotate
		if(rand() % ROTATION_CHANCE == 0)
			rot = (rand() % 1) ? 1 : -1;
	}
	rotateSolver(s, rot);
	return 0;
}

//Fill the maze with generated content
Vector2 genMaze(Maze *const maze){
	int startPos = (rand() % maze->size - EDGE_SIZE) + 1;
	Vector2 start = {0, 0};
	//Set start and randomly open paths in maze
	for(int x = 0; x < maze->size; x++){
		for(int y = 0; y < maze->size; y++){
			char placedChar = '*';
			//Entry point placement
			if(y == startPos && x == maze->size - 1){
				placedChar = ENTER;
				start.x = y;
				start.y = x;
			}
			else{
				if(rand() % EMPTY_SPACE_CHANCE == 0
						&&(x < maze->size - 1 && x > 1 &&
							y < maze->size - 1 && y > 1)){
					placedChar = EMPTY;
				}else{
					placedChar = WALL;
				}
			}
			maze->mazeData[x][y] = placedChar;
		}
	}

	Solver *pathCarver = newSolver(start, maze);
	pathCarver->position.y -= 1;
	//Carve out at least one path to the end
	int successPathFinished = 0;
	//pathCarver->verbose = 1;
	while(!successPathFinished)
		successPathFinished = carvePath(pathCarver, maze);
	freeSolver(pathCarver);

	return start;
}

int main(int argc, const char* argv[]){
	srand(time(NULL));
	int size;
	if(argc >= 2){
		size = strtol(argv[1], NULL, 10);
	}
	else{
		printf("Please enter the size for the maze:");
		scanf("%d", &size);
	}

	FILE *file;
	file = fopen( "maze.input", "w" );
	if(!file){
		printf("File could not be opened");
		return 1;
	}
	Maze *maze = newMaze(size);
	genMaze(maze);

	//Print file header
	fprintf(file, "%d\n", 1);
	fprintf(file, "%d\n", maze->size);
	//Print file body
	for(int x = 0; x < maze->size; x++){
		fprintf(file, "%s\n", maze->mazeData[x]);
	}

	printMaze(maze);
	freeMaze(maze);
	fclose( file );
}
