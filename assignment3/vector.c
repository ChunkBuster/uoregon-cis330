#include <stdio.h>
#include "vector.h"

Vector2 moveInDir(Vector2 v, const Direction d, const int distance){
    if(d == UP)
        v.y -= distance;
    if(d == RIGHT)
        v.x += distance;
    if(d == DOWN)
        v.y += distance;
    if(d == LEFT)
        v.x -= distance;
    return v;
}

Direction relativeDir(const Direction locality, const Direction d){
    return (Direction)((d + locality) % 4);
}

void printDir(const Direction d){
    if(d == UP)
        printf("UP\n");
    if(d == RIGHT)
        printf("RIGHT\n");
    if(d == DOWN)
        printf("DOWN\n");
    if(d == LEFT)
        printf("LEFT\n");
}
