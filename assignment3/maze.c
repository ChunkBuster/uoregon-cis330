#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include "mazedef.h"
#include "solver.h"

int main( int argc, const char* argv[] )
{
    if( argc < 2 ) {
        //checks for the input file name
        printf( "error; no input file name\n" );
        return 1;
    }
    //Optional, for printing solution path visually instead of as series of commands
    int solutionPathVis = 0;
    if (argc > 2){
        solutionPathVis = strcmp(argv[2], "-v") == 0 || strcmp(argv[2], "--vis") == 0? 1 : 0;
    }

    FILE *filePointer;
    filePointer = fopen( argv[1], "r" );

    int numberOfTestCases = 0;
    fscanf( filePointer, "%d\n", &numberOfTestCases );
    for( int testCaseNumber = 0; testCaseNumber < numberOfTestCases; testCaseNumber++ ) {
        int mazeSize = 0;

        fscanf( filePointer, "%d\n", &mazeSize );
        //Allocate and initialize memory for the program
        Maze *maze = newMaze(mazeSize);
        Vector2 startLocation = loadMaze(maze, filePointer);
        Solver *solver = newSolver(startLocation, maze);
        //Set the output type
        solver->verbose = ~solutionPathVis & 1;
        solver->drawPath = solutionPathVis;

        if(!solutionPathVis) printf( "ENTER\n" );
        int done = 0;
        while(!done){
            done = solveMaze(solver);
        }
        if(!solutionPathVis) printf( "EXIT\n***\n" );

        freeSolver(solver);
        if(solutionPathVis) printMaze(maze);
        freeMaze(maze);
    }
    fclose( filePointer );

    return 0;
}
