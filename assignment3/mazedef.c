#include <stdlib.h>
#include <stdio.h>
#include "mazedef.h"
#include "solver.h"

void allocateMaze(char ***mazeData, int size){
    *mazeData = malloc(sizeof *mazeData * size);
    for(int i = 0; i < size; i++){
        (*mazeData)[i] = malloc(sizeof *(mazeData[i]) * (size + 1));
        //Initialize the string
        (*mazeData)[i][size] = '\0';
    }
}

Maze *newMaze(const int size){
    Maze *m = (Maze*)malloc(sizeof(Maze));
    m->mazeData = NULL;
    m->size = size;
    allocateMaze(&m->mazeData, m->size);
    return m;
}

Vector2 loadMaze(const Maze *maze, FILE *filePointer){
    Vector2 entryPoint;
    //Load the maze into memory
    char* line = malloc(sizeof(char) * (maze->size + 1));
    for(int y = 0; y < maze->size; y++){
        fscanf(filePointer, "%s\n", line);
        for(int x = 0; x < maze->size; x++){
            char c = line[x];
            maze->mazeData[y][x] = c;
            //When the enter character is hit, store the position.
            if(c == ENTER){
                entryPoint.x = x;
                entryPoint.y = y;
            }
        }
    }
    free(line);
    return entryPoint;
}

void deallocateMaze(char ***mazeData, int size){
    for(int i = 0; i < size; i++){
        free((*mazeData)[i]);
    }
    free(*mazeData);
}

void freeMaze(Maze *maze){
    deallocateMaze(&maze->mazeData, maze->size);
    free(maze);
}

char charAtPos(const Maze *m, const Vector2 p){
    if(p.x >= m->size || p.y >= m->size || p.x < 0 || p.y < 0)
        return '\0';
    return m->mazeData[p.y][p.x];
}

void setCharAtPos(const Maze *m, const Vector2 p, const char c){
    if(p.x >= m->size || p.y >= m->size || p.x < 0 || p.y < 0)
        return;
    m->mazeData[p.y][p.x] = c;
}

char charInDir(const Maze *m, Vector2 p, const Direction d){
    p = moveInDir(p, d, 1);
    return charAtPos(m, p);
}

void printMaze(const Maze *m){
    for(int y = 0; y < m->size; y++){
        printf("%s\n", m->mazeData[y]);
    }
}
