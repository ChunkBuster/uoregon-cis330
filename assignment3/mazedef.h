#ifndef MAZE_DEF_H
#define MAZE_DEF_H
#include <stdio.h>
#include "vector.h"

#define WALL '@'
#define ENTER 'x'
#define BORDER '\0'
#define EMPTY '.'
#define TRAIL ' '

typedef struct{
    char** mazeData;
    int size;
}Maze;

//Allocate a new maze
Maze *newMaze(const int size);
//Free a maze
void freeMaze(Maze *maze);
//Load data into a maze from a given file pointer. Returns the starting point of the maze.
Vector2 loadMaze(const Maze *maze, FILE *filePointer);
//Get the character at a given position
char charAtPos(const Maze *m, const Vector2 p);
//Set the character at at given position
void setCharAtPos(const Maze *m, const Vector2 p, const char c);
//Get the character at a given position in a given direction
char charInDir(const Maze *m, Vector2 p, const Direction d);
//Print the maze to stdout
void printMaze(const Maze *m);
#endif //MAZE_DEF_H
