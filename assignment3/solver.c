#include <stdlib.h>
#include <stdio.h>
#include "solver.h"
#include "mazedef.h"

Solver *newSolver(const Vector2 initPos, Maze * const m){
    Solver *s = malloc(sizeof(Solver));
    s->position = initPos;
    s->targetMaze = m;
    s->currentDir = UP;
    s->stepsTaken = 0;
    s->verbose = 0;
    s->drawPath = 0;
    return s;
}

void freeSolver(Solver *s){
    free(s);
}

void rotateSolver(Solver *const s, const int rotation){
    s->currentDir = (s->currentDir + rotation) % 4;
}

void moveSolver(Solver *const s, const int moves){
    s->position = moveInDir(s->position, s->currentDir, moves);
    if(s->verbose){
        printDir(s->currentDir);
    }
    if(s->drawPath) setCharAtPos(s->targetMaze, s->position, TRAIL);
}

int solveMaze(Solver *const s){
    Vector2 currentNext = moveInDir(s->position, s->currentDir, 1);
    char currentNextChar = charAtPos(s->targetMaze, currentNext);

    Vector2 currentRight = moveInDir(s->position, relativeDir(s->currentDir, RIGHT), 1);
    char currentRightChar = charAtPos(s->targetMaze, currentRight);

    //If hugging a wall
    if(currentRightChar == WALL || currentRightChar == ENTER){
        //and the next space is empty
        if(currentNextChar == EMPTY || currentNextChar == TRAIL){
            moveSolver(s, 1);
            //make sure wall is still being touched
            Vector2 newRight = moveInDir(s->position, relativeDir(s->currentDir, RIGHT), 1);
            if(charAtPos(s->targetMaze, newRight) == EMPTY || charAtPos(s->targetMaze, newRight) == TRAIL) {
                //If there is now an empty space to the right, turn right and move forward.
                rotateSolver(s, 1);
                moveSolver(s, 1);
            }
        }else if(currentNextChar == WALL || currentNextChar == ENTER){
            //If facing a wall, turn left
            rotateSolver(s, 1);
        }else if(currentNextChar == BORDER){
            //If moving out of the maze, it is solved. Return success condition.
            //moveSolver(s, 1);
            return 1;
        }
    }else {
        rotateSolver(s, 1);
    }

    s->stepsTaken++;
    //Maze still unsolved
    return 0;
}
