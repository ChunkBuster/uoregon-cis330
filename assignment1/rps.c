//Sorry for the over-engineering, I'm using this as a chance to get back up to speed with C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef enum move {ROCK, PAPER, SCISSORS, INVALID} move;
typedef enum result {FIRST, SECOND, TIE} result;

//Turns a move into its string representation
char *move_to_str(move m){
	char *str = malloc(10);
	if(m == ROCK)
		strcpy(str, "rock");
	else if(m == PAPER)
		strcpy(str, "paper");
	else if(m == SCISSORS)
		strcpy(str, "scissors");
	return str;
}

//Turns a string into its move representation
move str_to_move(char *s){
	if(strcmp(s, "rock") == 0)
		return ROCK;
	if(strcmp(s, "paper") == 0)
		return PAPER;
	if(strcmp(s, "scissors") == 0)
		return SCISSORS;
	return INVALID;
}

//Determine the winning move in a match
result take_turn(move player_move, move cpu_move){
	if(player_move == cpu_move)
		return TIE;
	if(player_move == ROCK && cpu_move == SCISSORS)
		return FIRST;
	if(player_move == SCISSORS && cpu_move == PAPER)
		return FIRST;
	if(player_move == PAPER && cpu_move == ROCK)
		return FIRST;
	return SECOND;
}

//Print the result of a turn
void print_result(move player_move, move cpu_move){

	if(player_move == INVALID || cpu_move == INVALID){
		printf("Invalid input.");
		return;
	}

	result r = take_turn(player_move, cpu_move);

	char *p_mov_str = move_to_str(player_move);
	char *c_mov_str = move_to_str(cpu_move);

	printf("You picked %s.\nComputer picked %s.\n", p_mov_str, c_mov_str);

	if(r == TIE){
		printf("This game is a tie");
		goto FREE;
	}

	char *winner = (r == FIRST) ? p_mov_str: c_mov_str;
	printf("%s Wins.\n", winner);

FREE:
	free(p_mov_str);
	free(c_mov_str);
}

int main(char* args[]){
	char *input = malloc(20);
	printf("Enter rock, paper, or scissors: ");
	scanf("%s", input);
	print_result(str_to_move(input), ROCK);
	free(input);
}
