//
// Created by Mason on 3/6/2018.
//


#include <sstream>     	// std::istringstream
#include <fstream>
#include <map>		// std::multimap
#include <algorithm>   	// std::copy, std::for_each

#include "mr.hpp"
#include "ioutils.hpp"
#include "wordCount.hpp"

#include "SentenceStats.hpp"
namespace mr {

void SentenceStats::MRmap(const std::map<std::string, std::string> &input,
                          std::multimap<SentenceType,int> &out_values) {
    IOUtils io;
    // input: in a real Map Reduce application the input will also be a map
    // where the key is a file name or URL, and the value is the document's contents.
    // Here we just take the string input and process it.
    for (auto it = input.begin(); it != input.end(); it++ ) {
        std::string inputString = io.readFromFile(it->first);
        std::istringstream iss(inputString);
        int sentenceLength = 0;
        do {
            std::string word;
            iss >> word;
            sentenceLength++;
            for (unsigned i = 0, s = word.size(); i < s; i++){
                if (sentenceLength != 0 && (word[i] == '.' || word [i] == '!'|| word [i] == '?'|| word [i] == '\n'))
                {
                    out_values.insert(std::pair<SentenceType,int>(SentenceType::Largest,sentenceLength));
                    out_values.insert(std::pair<SentenceType,int>(SentenceType::Smallest,sentenceLength));
                    out_values.insert(std::pair<SentenceType,int>(SentenceType::Average,sentenceLength));
                    sentenceLength = 0;
                    break;
                }
            }

        } while (iss);
    }
}

void SentenceStats::MRreduce(const std::multimap<SentenceType, int> &intermediate_values,
                             std::map<SentenceType, int> &out_values) {


    std::for_each(intermediate_values.begin(), intermediate_values.end(),
            // Anonymous function that increments the sum for each unique key (word)
                  [&](std::pair<SentenceType,int> mapElement)->void
                  {
                      if(mapElement.second > out_values[SentenceType::Largest])
                          out_values[SentenceType::Largest] = mapElement.second;
                      if( out_values[SentenceType::Smallest] == 0 || mapElement.second < out_values[SentenceType::Smallest])
                          out_values[SentenceType::Smallest] = mapElement.second;
                      out_values[SentenceType::Average] += mapElement.second;
                  });  // end of for_each
    out_values[SentenceType::Average] /= intermediate_values.size();
}
}