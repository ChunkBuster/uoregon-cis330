//
// Created by Mason on 3/6/2018.
//

#ifndef ASSIGNMENT6_SENTENCESTATS_H
#define ASSIGNMENT6_SENTENCESTATS_H

#include <fstream>
#include <string>
#include <map>

#include "mr.hpp"

enum SentenceType{
    Smallest, Largest, Average
};
namespace mr {

    class SentenceStats : public mr::MapReduce<SentenceType, int> {
        virtual void MRmap(const std::map<std::string, std::string> &input,
                           std::multimap<SentenceType, int> &out_values);

        virtual void MRreduce(const std::multimap<SentenceType, int> &intermediate_values,
                              std::map<SentenceType, int> &out_values);
    };
}


#endif //ASSIGNMENT6_SENTENCESTATS_H
