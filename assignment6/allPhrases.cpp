//
// Created by Mason on 3/6/2018.
//


#include <sstream>     	// std::istringstream
#include <fstream>
#include <map>		// std::multimap
#include <algorithm>   	// std::copy, std::for_each

#include "mr.hpp"
#include "ioutils.hpp"

#include "allPhrases.hpp"

namespace mr {

    //Normalizes and returns all of the words until then next punctuation mark.
    std::vector<std::string> wordsInNextSentence(std::istringstream &stream){
        std::vector<std::string> sentenceVec;
        do {
            std::string word;
            stream >> word;
            bool ended = false;
            for (int i = 0, s = word.size(); i < s; i++) {
                char curr = word[i];
                if (curr == '.' || curr == '?' ||
                    curr == ',' || curr == ':' ||
                    curr == '!' || curr == ';' ||
                    curr == ')' || curr == '(' ||
                    curr == '\n') {
                    word.erase(i, 1);
                    s = word.size();
                    i--;
                    ended = true;
                    continue;
                }
                word[i] = tolower(curr);
                if(ended)
                    return sentenceVec;
            }
            sentenceVec.push_back(word);
        }while(stream);
        return sentenceVec;
    }

    std::vector<std::string> wordPhrasePowerset(std::vector<std::string> &sentenceVec){
        std::vector<std::string> powerVec;
        for(auto i = sentenceVec.begin(); i != sentenceVec.end(); ++i){
            powerVec.push_back(*i);
            for(auto u = i + 1; u != sentenceVec.end(); ++u){
                std::string newPhrase = powerVec.back() + ' ' + *u;
                powerVec.push_back(newPhrase);
            }
        }
        return powerVec;
    }
    void AllPhrases::MRmap(const std::map<std::string,std::string> &input,
                   std::multimap<std::string,int> &out_values) {
        IOUtils io;
        // input: in a real Map Reduce application the input will also be a map
        // where the key is a file name or URL, and the value is the document's contents.
        // Here we just take the string input and process it.
        for (auto it = input.begin(); it != input.end(); it++ ) {
            std::string inputString = io.readFromFile(it->first);

            // Split up the input into words
            std::istringstream iss(inputString);
            do {
                std::vector<std::string> words = wordsInNextSentence(iss);
                std::vector<std::string> set = wordPhrasePowerset(words);
                for(auto i = set.begin(); i != set.end(); ++i){
                    out_values.insert(std::pair<std::string,int>(*i,1));
                }

            } while (iss);
        }
    }

// A specialized reduce function with string keys and int value
    void
    AllPhrases::MRreduce(const std::multimap<std::string,int> &intermediate_values,
                      std::map<std::string,int> &out_values) {

        // Sum up the counts for all intermediate_values with the same key
        // The result is the out_values map with each unique word as
        // the key and a total count of occurrences of that word as the value.
        std::for_each(intermediate_values.begin(), intermediate_values.end(),
                // Anonymous function that increments the sum for each unique key (word)
                      [&](std::pair<std::string,int> mapElement)->void
                      {
                          out_values[mapElement.first] += 1;
                      });  // end of for_each
    }

}; // namespace mr
