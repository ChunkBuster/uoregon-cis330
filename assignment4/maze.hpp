/*
 * maze.hpp
 *
 *  Created on: Jan 27, 2014
 *      Author: norris
 */
#pragma once
#include "vector2.hpp"
#include <fstream>
#include <vector>

#define WALL (char)'@'
#define ENTER (char)'x'
#define BORDER (char)'\0'
#define EMPTY (char)'.'
#define TRAIL (char)' '
#define DELIMITER "***"

class Maze
{
public:
    Maze(int size);
    ~Maze();

    //! read maze from file, find starting location
    //! \param f filestream
    void readFromFile(std::ifstream &f);

    //! make a single step advancing toward the exit
    void step();

    //! return true if the maze exit has been reached, false otherwise
    bool atExit();

    //! set row and col to current _position of 'x'
    //! \param row out val for row
    //! \param col out val for pos
    void getCurrentPosition(int &row, int &col);

    //! Get the char at a given position
    //! \param p position to check
    //! \return character at positon
    char charAtPos(dir::Vector2 p);

    //! Return a string representation of the current state of the maze
    //! \return the string representation
    std::string toString();

private:
    // Private methods
    bool atEdge(int x, int y);
    char getValue(int x, int y);
    bool isValidMove(int x, int y);

    void move(int distance);
    void rotate(int distance);
    bool solve();

    // Private data

    int _size, xStart, yStart, row, col; // _size and _position information
    std::string _mazeData; // where the maze will be stored
    bool _solved;
    dir::Direction _currentDir;  // direction information
    int _numberOfMoves;  // for counting number of moves in solution
    dir::Vector2 _position;

    std::vector<std::vector<char>> _mazeChars;
};



