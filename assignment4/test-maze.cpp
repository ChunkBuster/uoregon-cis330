#include "maze.hpp"
#include <iostream>

inline void printDelim(){
    std::cout << DELIMITER << '\n';
}

int main(int argc, char** argv){
    if(argc < 2){
        std::cerr << "Please pass an input text file to the program";
        return 1;
    }

    std::ifstream f (argv[1], std::ifstream::in);

    if(!f){
        std::cerr << "Input file " << argv[1] << "not found!";
    }

    int numMazes;
    f >> numMazes;

    for(int i = 0; i < numMazes; i++){
        int size;
        f >> size;
        Maze maze(size);
        maze.readFromFile(f);
        std::cout << "ENTER" << std::endl;
        while(!maze.atExit()){
            maze.step();
        }
        std::cout << "EXIT" << std::endl;
        printDelim();
        //std::cout << maze.toString() << std::endl;

    }
}
