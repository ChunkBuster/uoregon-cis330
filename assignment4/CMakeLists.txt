cmake_minimum_required(VERSION 3.0)
set(CMAKE_CXX_STANDARD 11)
# Use the flags below for all builds and report the build type
set(CMAKE_CXX_FLAGS "-g -Wall")
message("CMAKE_BUILD_TYPE is ${CMAKE_BUILD_TYPE}")

set(MAZE_SOURCES test-maze.cpp maze.cpp vector2.cpp)

add_executable(test-maze.exe ${MAZE_SOURCES})

target_compile_definitions(test-maze.exe PRIVATE "")
