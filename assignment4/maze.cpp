#include "maze.hpp"
#include <iostream>

Maze::Maze(int size) : _size(size), _mazeData(), _solved(false), _currentDir(dir::UP), _numberOfMoves(0){
    _mazeChars.resize(size, std::vector<char>(size, '\0'));
}

Maze::~Maze(){ }

void Maze::readFromFile(std::ifstream &f){
    char c;
    int iter = 0;
    dir::Vector2 position;
    dir::Vector2 startPos;

    for(int i = 0; i < this->_size * this->_size; i++){
        position.x = iter % this->_size;
        position.y = iter / this->_size;
        f >> c;

        if(c == '\n' || c == '\r')
            continue;

        if(c == ENTER)
            startPos = position;

        if(iter != 0 && position.x == 0){
            _mazeData += '\n';
        }

        _mazeData += c;
        _mazeChars[position.x][position.y] = c;
        iter++;
    }
    //startPos.x += 1;
    //startPos.y += 1;
    //std::cout << startPos.x << " " << startPos.y;
    xStart = startPos.y;
    yStart = startPos.x;
    _position = dir::Vector2(yStart, xStart);
}

void Maze::step(){
    if(!_solved){
        _solved = solve();
    }
}

bool Maze::atExit(){
    return _solved;
}

void Maze::getCurrentPosition(int &row, int &column){
    row = _position.x;
    column = _position.y;
}

char Maze::charAtPos(dir::Vector2 p){
    if(isValidMove(p.x, p.y))
        return  _mazeChars[p.x][p.y];
    else
        return BORDER;
}

void Maze::move(int distance){
    dir::Vector2 newPos = dir::moveInDir(_position, _currentDir, distance);
    if(isValidMove(newPos.x, newPos.y))
        _position = dir::moveInDir(_position, _currentDir, distance);
    dir::printDir(_currentDir);
}

void Maze::rotate(int rotation){
    _currentDir = (dir::Direction)((_currentDir + rotation) % 4);
}

bool Maze::solve(){
    dir::Vector2 currentNext = dir::moveInDir(_position, _currentDir, 1);
    char currentNextChar = charAtPos(currentNext);
    //std::cout << "Next char: " << currentNextChar << std::endl;
    dir::Vector2 currentRight = dir::moveInDir(_position, dir::relativeDir(_currentDir, dir::RIGHT), 1);
    char currentRightChar = charAtPos(currentRight);
    //std::cout << "Right char: " << currentRightChar << std::endl;
    //If hugging a wall
    if(currentRightChar == WALL || currentRightChar == ENTER){
        //and the next space is empty
        if(currentNextChar == EMPTY){
            move(1);
            //make sure wall is still being touched
            dir::Vector2 newRight = dir::moveInDir(_position, dir::relativeDir(_currentDir, dir::RIGHT), 1);
            if(charAtPos(newRight) == EMPTY) {
                //If there is now an empty space to the right, turn right and move forward.
                rotate(1);
                move(1);
            }
        }else if(currentNextChar == WALL || currentNextChar == ENTER){
            //If facing a wall, turn left
            rotate(1);
        }else if(currentNextChar == BORDER){
            //If moving out of the maze, it is _solved. Return success condition.
            return true;
        }
    }else {
        rotate(1);
    }
    _numberOfMoves++;
    //Maze still unsolved
    return false;

}

std::string Maze::toString(){
    std::string outStr;
    for(int y = 0; y < _size; y++){
        for(int x = 0; x < _size; x++){
            outStr += _position.x == x && _position.y == y ? ENTER : _mazeChars[x][y];
        }
        outStr += '\n';
    }
    return outStr;
}

bool Maze::atEdge(int x, int y) {
    return (x == 0 || y == 0 || x == _size - 1 || y == _size - 1);
}


char Maze::getValue(int x, int y){
    return(charAtPos(dir::Vector2(x, y)));
}

bool Maze::isValidMove(int x, int y){
    return (x >= 0 && y >=0 && x < _size && y < _size);
}
