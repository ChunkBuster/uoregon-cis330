#include <iostream>
#include "vector2.hpp"

namespace dir{
    Vector2 moveInDir(Vector2 v, const Direction d, const int distance){
        if(d == UP)
            v.y -= distance;
        if(d == RIGHT)
            v.x += distance;
        if(d == DOWN)
            v.y += distance;
        if(d == LEFT)
            v.x -= distance;
        return v;
    }

    Direction relativeDir(const Direction locality, const Direction d){
        return (Direction)((d + locality) % 4);
    }

    void printDir(const Direction d){
        std::string toPrint;
        if(d == UP)
            toPrint = "UP";
        if(d == RIGHT)
            toPrint ="RIGHT";
        if(d == DOWN)
            toPrint ="DOWN";
        if(d == LEFT)
            toPrint ="LEFT";
        std::cout << toPrint << '\n';
    }

}
