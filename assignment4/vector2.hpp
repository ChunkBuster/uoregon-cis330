#pragma once

namespace dir{
    //Enum representing direction
    typedef enum Direction { UP, RIGHT, DOWN, LEFT} Direction;

    typedef struct Vector2{
        int x;
        int y;

        Vector2(int x, int y){
            this->x = x;
            this->y = y;
        }

        Vector2(Vector2 *other){
            x = other->x;
            y = other->y;
        }

        Vector2(){
            this->x = 0;
            this->y = 0;
        }

        Vector2 operator+(Vector2& rhs){
            return Vector2(this->x + rhs.x, this->y + rhs.y);
        }

    } Vector2;

    //! Get the absolute direction given a direction relative to locality
    //! \param locality Locality to calculate from
    //! \param d direction to transform
    //! \return relative dir
    Direction relativeDir(const Direction locality, const Direction d);

    //! Print the string representation of a dir to stdout
    //! \param d Direction to print
    void printDir(const Direction d);

    //! Move a Vector2 in a given direction, then return the new vector
    //! \param v Vector2 to move
    //! \param d
    //! \param distance
    //! \return
    Vector2 moveInDir(Vector2 v, const Direction d, const int distance);
}

