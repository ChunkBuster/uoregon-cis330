#include "SimpleString.hpp"

// Include other headers as necessary

#define ITERS 10

int main(int argc, const char *argv[])
{
    SimpleString s1("Hello ");
    SimpleString s2("World!");
    SimpleString s3;
     
    //Here is some sample usage of the functions that you're
    //implementing; uncomment the lines to test your code.

    std::cout << s1 << std::endl; //Hello
    std::cout << s2 << std::endl; //World!
    s3 = s1 + s2;
    s2 = s1;
    std::cout << s2 << std::endl; //World!
    std::cout << s3 << std::endl;
    for(int i = 0; i < ITERS; i++)
        std::cout << ++s3 << std::endl;
    for(int i = 0; i < ITERS; i++)
        std::cout << --s3 << std::endl;
}
