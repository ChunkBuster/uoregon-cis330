#include "SimpleString.hpp"

// Include any other header files as necessary


// Define and implement the SimpleString class methods in this file…

// The 2 constructors are defined for you:

SimpleString::SimpleString() {
    this->input = "Default value";
}

SimpleString::SimpleString(std::string s) {
    this->input = s;
}

// TODO: Define the getter/setter and operator overloading methods…


void SimpleString::setString(std::string str){
    input = str;
}

std::string SimpleString::getString(){
    return input;
}

SimpleString& SimpleString::operator=(const SimpleString & other){
    setString(other.input);
    return *this;
}

SimpleString SimpleString::operator+(const SimpleString & other){
    return SimpleString(this->input + other.input);
}

SimpleString& SimpleString::operator++(){
    input.append("*");
    return *this;
}


SimpleString& SimpleString::operator--(){
    input.pop_back();
    return *this;
}

std::ostream& operator<<(std::ostream& os, const SimpleString& s){
    os << s.input;
    return os;
}

std::istream& operator>>(std::istream& is, SimpleString& s){
    s.input.append(s.input);
    return is;
}






